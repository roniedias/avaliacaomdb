package br.com.fiap.mdb;

import java.io.PrintWriter;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven(activationConfig = { @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"), 
									@ActivationConfigProperty(propertyName= "destination", propertyValue = "queue/AvaliacaoEJBQueue") })

public class QueueListenerMDB implements MessageListener {
	
    public QueueListenerMDB() {}
    	
    public void onMessage(Message message) {
    	
    	TextMessage msg = (TextMessage) message;
    	PrintWriter writer = null;    	
    	try {
    		writer = new PrintWriter("/Users/roniedias/Desktop/sorteio.txt", "UTF-8");
    		writer.print(msg.getText());
    	} 
    	catch (Exception e) {
    		e.printStackTrace();
		}
    	writer.close();    			
    }
}